# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the dialect package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: dialect\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-06-27 16:36-0500\n"
"PO-Revision-Date: 2021-10-07 04:04+0000\n"
"Last-Translator: Noboru Higuchi <kikutiyo@gmail.com>\n"
"Language-Team: Japanese <https://hosted.weblate.org/projects/dialect/dialect/"
"ja/>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 4.9-dev\n"

#. Translators: Do not translate the app name!
#: data/app.drey.Dialect.desktop.in.in:8
#: data/app.drey.Dialect.metainfo.xml.in.in:6 data/resources/about.blp:6
#: data/resources/window.blp:240 dialect/main.py:57
msgid "Dialect"
msgstr "Dialect"

#: data/app.drey.Dialect.desktop.in.in:9
#: data/app.drey.Dialect.metainfo.xml.in.in:7
msgid "Translate between languages"
msgstr "言語を翻訳"

#. Translators: These are search terms to find this application. Do NOT translate or localize the semicolons. The list MUST also end with a semicolon.
#: data/app.drey.Dialect.desktop.in.in:13
msgid "translate;translation;"
msgstr "翻訳元;翻訳先;"

#: data/app.drey.Dialect.metainfo.xml.in.in:9 data/resources/about.blp:8
msgid "A translation app for GNOME."
msgstr "GNOME のための翻訳アプリケーション。"

#: data/app.drey.Dialect.metainfo.xml.in.in:12
msgid "Features:"
msgstr "機能:"

#: data/app.drey.Dialect.metainfo.xml.in.in:16
#, fuzzy
msgid "Translation based on Google Translate"
msgstr "非公式の Google 翻訳 API による翻訳"

#: data/app.drey.Dialect.metainfo.xml.in.in:17
#, fuzzy
msgid ""
"Translation based on the LibreTranslate API, allowing you to use any public "
"instance"
msgstr ""
"LibreTranslate API による翻訳, 利用可能なオンラインインスタンスを使用します"

#: data/app.drey.Dialect.metainfo.xml.in.in:18
#, fuzzy
msgid "Translation based on Lingva Translate API"
msgstr "非公式の Google 翻訳 API による翻訳"

#: data/app.drey.Dialect.metainfo.xml.in.in:19
msgid "Text to speech"
msgstr "Text to speech"

#: data/app.drey.Dialect.metainfo.xml.in.in:20
msgid "Translation history"
msgstr "翻訳履歴"

#: data/app.drey.Dialect.metainfo.xml.in.in:21
msgid "Automatic language detection"
msgstr "言語の自動判定"

#: data/app.drey.Dialect.metainfo.xml.in.in:22
msgid "Clipboard buttons"
msgstr "クリップボードボタン"

#: data/app.drey.Dialect.metainfo.xml.in.in:28
msgid "The Dialect Authors"
msgstr "Dialect の作者"

#: data/resources/about.blp:7
#, fuzzy
msgid "Copyright 2020–⁠2022 The Dialect Authors"
msgstr "Copyright 2020–⁠2021 The Dialect Authors"

#: data/resources/about.blp:10
msgid "GitHub page"
msgstr "GitHub ページ"

#. Translators: Replace me with your names.
#: data/resources/about.blp:14
msgid "translator-credits"
msgstr "Noboru Higuchi"

#: data/resources/preferences.ui:11
msgid "Behavior"
msgstr "動作"

#: data/resources/preferences.ui:14
msgid "Live Translation"
msgstr "ライブ翻訳"

#: data/resources/preferences.ui:15
msgid "Warning: Your IP address may get banned for API abuse."
msgstr ""
"注意 : あなたの IP アドレスが API の乱用により接続拒否される可能性がありま"
"す。"

#: data/resources/preferences.ui:21
#, fuzzy
msgid "Show Translations in Desktop Search"
msgstr "翻訳履歴"

#: data/resources/preferences.ui:22
msgid "Warning: All desktop searches will be sent to the translation service"
msgstr ""

#: data/resources/preferences.ui:35
msgid "Translation Shortcut"
msgstr "翻訳実行のショートカットキー"

#: data/resources/preferences.ui:36
msgid "The unselected choice will be used for line break."
msgstr "選択されなかったキー組み合わせは改行入力になります。"

#: data/resources/preferences.ui:49
msgid "Default to Auto"
msgstr "自動判別をデフォルトにする"

#: data/resources/preferences.ui:50
msgid "Use \"Auto\" as the default language"
msgstr "”自動判別\" をデフォルトの翻訳元言語にする"

#: data/resources/preferences.ui:61
msgid "Translator"
msgstr "翻訳"

#: data/resources/preferences.ui:62
msgid "Choose from the available translation services."
msgstr "利用可能な翻訳サービスを選択します。"

#: data/resources/preferences.ui:70
msgid "Translator Instance"
msgstr "Translator インスタンス"

#: data/resources/preferences.ui:71
msgid "Enter a translation service URL."
msgstr "翻訳サービスの URL を指定します。"

#: data/resources/preferences.ui:89 data/resources/preferences.ui:161
msgid "Edit"
msgstr "編集"

#: data/resources/preferences.ui:116 data/resources/preferences.ui:188
msgid "Reset to default"
msgstr "既定値にリセット"

#: data/resources/preferences.ui:126 data/resources/preferences.ui:198
#: data/resources/window.blp:524
msgid "Save"
msgstr "保存"

#: data/resources/preferences.ui:142
msgid "API Key"
msgstr ""

#: data/resources/preferences.ui:143
#, fuzzy
msgid "Enter an API key for the translation service."
msgstr "翻訳サービスの URL を指定します。"

#: data/resources/preferences.ui:214
msgid "Text-to-Speech"
msgstr "Text-to-Speech"

#: data/resources/preferences.ui:215
msgid "Use Google for TTS."
msgstr "TTS に Google を使用します。"

#: data/resources/shortcuts.blp:13
msgctxt "shortcuts window"
msgid "Translator"
msgstr "翻訳"

#: data/resources/shortcuts.blp:17
msgctxt "shortcuts window"
msgid "Translate"
msgstr "翻訳"

#: data/resources/shortcuts.blp:23
msgctxt "shortcuts window"
msgid "Switch Languages"
msgstr "言語の切り替え"

#: data/resources/shortcuts.blp:29
msgctxt "shortcuts window"
msgid "Clear source text"
msgstr "翻訳元テキストをクリア"

#: data/resources/shortcuts.blp:35
msgctxt "shortcuts window"
msgid "Copy translation"
msgstr "翻訳結果をコピー"

#: data/resources/shortcuts.blp:41
msgctxt "shortcuts window"
msgid "Show Pronunciation"
msgstr "発音を表示"

#: data/resources/shortcuts.blp:48
msgctxt "shortcuts window"
msgid "Text-to-Speech"
msgstr "Text-to-Speech"

#: data/resources/shortcuts.blp:52
msgctxt "shortcuts window"
msgid "Listen to source text"
msgstr "翻訳元テキストを聞く"

#: data/resources/shortcuts.blp:58
msgctxt "shortcuts window"
msgid "Listen to translation"
msgstr "翻訳結果を聞く"

#: data/resources/shortcuts.blp:65
msgctxt "shortcuts window"
msgid "Navigation"
msgstr "操作"

#: data/resources/shortcuts.blp:69
msgctxt "shortcuts window"
msgid "Go back in history"
msgstr "前の履歴"

#: data/resources/shortcuts.blp:75
msgctxt "shortcuts window"
msgid "Go forward in history"
msgstr "次の履歴"

#: data/resources/shortcuts.blp:82
msgctxt "shortcuts window"
msgid "General"
msgstr "一般"

#: data/resources/shortcuts.blp:86
msgctxt "shortcuts window"
msgid "Preferences"
msgstr "設定"

#: data/resources/shortcuts.blp:92
msgctxt "shortcut window"
msgid "Shortcuts"
msgstr "ショートカット"

#: data/resources/shortcuts.blp:98
msgctxt "shortcuts window"
msgid "Quit"
msgstr "終了"

#: data/resources/window.blp:15
msgid "Show Pronunciation"
msgstr "発音を表示"

#: data/resources/window.blp:24 dialect/window.py:1224 dialect/window.py:1232
msgid "Preferences"
msgstr "設定"

#: data/resources/window.blp:29
msgid "Keyboard Shortcuts"
msgstr "ショートカットキー"

#: data/resources/window.blp:34
msgid "About Dialect"
msgstr "Dialect について"

#: data/resources/window.blp:70
msgid "Loading…"
msgstr "読み込み中…"

#: data/resources/window.blp:103 dialect/window.py:375 dialect/window.py:1216
#: dialect/window.py:1240
msgid "Retry"
msgstr "リトライ"

#: data/resources/window.blp:112 data/resources/window.blp:163
#, fuzzy
msgid "Open Preferences"
msgstr "設定を開く"

#: data/resources/window.blp:154
msgid "Remove Key and Retry"
msgstr ""

#: data/resources/window.blp:215 data/resources/window.blp:572
msgid "Switch languages"
msgstr "言語を切り替える"

#: data/resources/window.blp:251
msgid "Previous translation"
msgstr "前の翻訳結果"

#: data/resources/window.blp:257
msgid "Next translation"
msgstr "次の翻訳結果"

#: data/resources/window.blp:366
msgid "Clear"
msgstr "クリア"

#: data/resources/window.blp:372
msgid "Paste"
msgstr "貼り付け"

#: data/resources/window.blp:378 data/resources/window.blp:496
msgid "Listen"
msgstr "発音を聞く"

#: data/resources/window.blp:398
msgid "Translate"
msgstr "翻訳"

#: data/resources/window.blp:470
msgid "Translating…"
msgstr "翻訳中です…"

#: data/resources/window.blp:475
msgid "Could not translate the text"
msgstr "翻訳できません"

#: data/resources/window.blp:482
msgid "Copy"
msgstr "コピー"

#: data/resources/window.blp:490
#, fuzzy
msgid "Suggest Translation"
msgstr "ライブ翻訳"

#: data/resources/window.blp:517
msgid "Cancel"
msgstr ""

#: data/resources/lang-selector.blp:18
#, fuzzy
msgid "Search Languages…"
msgstr "言語の切り替え"

#: data/resources/theme-switcher.blp:25
msgid "Follow system style"
msgstr ""

#: data/resources/theme-switcher.blp:39
msgid "Light style"
msgstr ""

#: data/resources/theme-switcher.blp:53
#, fuzzy
msgid "Dark style"
msgstr "ダークモード"

#: dialect/preferences.py:220
#, python-brace-format
msgid "Not a valid {backend} instance"
msgstr "無効な {backend} インスタンス"

#: dialect/preferences.py:288
#, fuzzy, python-brace-format
msgid "Not a valid {backend} API key"
msgstr "無効な {backend} インスタンス"

#: dialect/window.py:296 dialect/window.py:1222
msgid "The provided API key is invalid"
msgstr ""

#: dialect/window.py:298
msgid "Please set a valid API key in the preferences."
msgstr ""

#: dialect/window.py:301
msgid "Please set a valid API key or unset the API key in the preferences."
msgstr ""

#: dialect/window.py:322 dialect/window.py:1230
msgid "API key is required to use the service"
msgstr ""

#: dialect/window.py:323
msgid "Please set an API key in the preferences."
msgstr ""

#: dialect/window.py:336
#, fuzzy
msgid "Failed loading the translation service"
msgstr "翻訳サービスを使用できません"

#: dialect/window.py:337
msgid "Please report this in the Dialect bug tracker if the issue persists."
msgstr ""

#: dialect/window.py:339
#, python-brace-format
msgid ""
"Failed loading \"{url}\", check if the instance address is correct or report "
"in the Dialect bug tracker if the issue persists."
msgstr ""

#: dialect/window.py:343
#, fuzzy
msgid "Couldn’t connect to the translation service"
msgstr "翻訳サービスを使用できません"

#: dialect/window.py:344
#, fuzzy
msgid "We can’t connect to the server. Please check for network issues."
msgstr ""
"翻訳に失敗しました。\n"
"ネットワーク接続を確認してください。"

#: dialect/window.py:346
#, python-brace-format
msgid ""
"We can’t connect to the {service} instance \"{url}\".\n"
" Please check for network issues or if the address is correct."
msgstr ""

#: dialect/window.py:369
msgid "A network issue has occured. Retry?"
msgstr "ネットワーク接続で問題が発生しました。リトライしますか？"

#: dialect/window.py:382
#, fuzzy
msgid "A network issue has occured. Please try again."
msgstr ""
"ネットワーク接続で問題が発生しました。\n"
"再度お試しください。"

#: dialect/window.py:666 dialect/window.py:673 dialect/window.py:1126
msgid "Auto"
msgstr "自動判別"

#: dialect/window.py:870
msgid "New translation has been suggested!"
msgstr ""

#: dialect/window.py:872
msgid "Suggestion failed."
msgstr ""

#: dialect/window.py:1012
#, fuzzy
msgid "{} characters limit reached!"
msgstr "5000 文字の制限に達しました！"

#: dialect/window.py:1165
msgid "Did you mean: "
msgstr "もしかして : "

#: dialect/window.py:1214
#, fuzzy
msgid "Translation failed, check for network issues"
msgstr ""
"翻訳に失敗しました。\n"
"ネットワーク接続を確認してください。"

#: dialect/window.py:1238
#, fuzzy
msgid "Translation failed"
msgstr "翻訳履歴"

#: dialect/translators/__init__.py:26
msgid "Afrikaans"
msgstr "アフリカーンス語"

#: dialect/translators/__init__.py:27
msgid "Albanian"
msgstr "アルバニア語"

#: dialect/translators/__init__.py:28
msgid "Amharic"
msgstr "アムハラ語"

#: dialect/translators/__init__.py:29
msgid "Arabic"
msgstr "アラビア語"

#: dialect/translators/__init__.py:30
msgid "Armenian"
msgstr "アルメニア語"

#: dialect/translators/__init__.py:31
msgid "Azerbaijani"
msgstr "アゼルバイジャン語"

#: dialect/translators/__init__.py:32
msgid "Basque"
msgstr "バスク語"

#: dialect/translators/__init__.py:33
msgid "Belarusian"
msgstr "ベラルーシ語"

#: dialect/translators/__init__.py:34
msgid "Bengali"
msgstr "ベンガル語"

#: dialect/translators/__init__.py:35
msgid "Bosnian"
msgstr "ボスニア語"

#: dialect/translators/__init__.py:36
msgid "Bulgarian"
msgstr "ブルガリア語"

#: dialect/translators/__init__.py:37
msgid "Catalan"
msgstr "カタロニア語"

#: dialect/translators/__init__.py:38
msgid "Cebuano"
msgstr "セブアノ語"

#: dialect/translators/__init__.py:39
msgid "Chichewa"
msgstr "チェワ語"

#: dialect/translators/__init__.py:40
msgid "Chinese"
msgstr "中国語"

#: dialect/translators/__init__.py:41
msgid "Chinese (Simplified)"
msgstr "中国語（簡体字）"

#: dialect/translators/__init__.py:42
msgid "Chinese (Traditional)"
msgstr "中国語（繁体字）"

#: dialect/translators/__init__.py:43
msgid "Corsican"
msgstr "コルシカ語"

#: dialect/translators/__init__.py:44
msgid "Croatian"
msgstr "クロアチア語"

#: dialect/translators/__init__.py:45
msgid "Czech"
msgstr "チェコ語"

#: dialect/translators/__init__.py:46
msgid "Danish"
msgstr "デンマーク語"

#: dialect/translators/__init__.py:47
msgid "Dutch"
msgstr "オランダ語"

#: dialect/translators/__init__.py:48
msgid "English"
msgstr "英語"

#: dialect/translators/__init__.py:49
msgid "Esperanto"
msgstr "エスペラント語"

#: dialect/translators/__init__.py:50
msgid "Estonian"
msgstr "エストニア語"

#: dialect/translators/__init__.py:51
msgid "Filipino"
msgstr "フィリピン語"

#: dialect/translators/__init__.py:52
msgid "Finnish"
msgstr "フィンランド語"

#: dialect/translators/__init__.py:53
msgid "French"
msgstr "フランス語"

#: dialect/translators/__init__.py:54
msgid "Frisian"
msgstr "フリジア語"

#: dialect/translators/__init__.py:55
msgid "Galician"
msgstr "ガリシア語"

#: dialect/translators/__init__.py:56
msgid "Georgian"
msgstr "ジョージア（グルジア）語"

#: dialect/translators/__init__.py:57
msgid "German"
msgstr "ドイツ語"

#: dialect/translators/__init__.py:58
msgid "Greek"
msgstr "ギリシャ語"

#: dialect/translators/__init__.py:59
msgid "Gujarati"
msgstr "グジャラート語"

#: dialect/translators/__init__.py:60
msgid "Haitian Creole"
msgstr "ハイチ語"

#: dialect/translators/__init__.py:61
msgid "Hausa"
msgstr "ハウサ語"

#: dialect/translators/__init__.py:62
msgid "Hawaiian"
msgstr "ハワイ語"

#: dialect/translators/__init__.py:63 dialect/translators/__init__.py:64
msgid "Hebrew"
msgstr "ヘブライ語"

#: dialect/translators/__init__.py:65
msgid "Hindi"
msgstr "ヒンディー語"

#: dialect/translators/__init__.py:66
msgid "Hmong"
msgstr "モン語"

#: dialect/translators/__init__.py:67
msgid "Hungarian"
msgstr "ハンガリー語"

#: dialect/translators/__init__.py:68
msgid "Icelandic"
msgstr "アイスランド語"

#: dialect/translators/__init__.py:69
msgid "Igbo"
msgstr "イボ語"

#: dialect/translators/__init__.py:70
msgid "Indonesian"
msgstr "インドネシア語"

#: dialect/translators/__init__.py:71
msgid "Irish"
msgstr "アイルランド語"

#: dialect/translators/__init__.py:72
msgid "Italian"
msgstr "イタリア語"

#: dialect/translators/__init__.py:73
msgid "Japanese"
msgstr "日本語"

#: dialect/translators/__init__.py:74
msgid "Javanese"
msgstr "ジャワ語"

#: dialect/translators/__init__.py:75
msgid "Kannada"
msgstr "カンナダ語"

#: dialect/translators/__init__.py:76
msgid "Kazakh"
msgstr "カザフ語"

#: dialect/translators/__init__.py:77
msgid "Khmer"
msgstr "クメール語"

#: dialect/translators/__init__.py:78
msgid "Kinyarwanda"
msgstr "ルワンダ語"

#: dialect/translators/__init__.py:79
msgid "Korean"
msgstr "韓国語"

#: dialect/translators/__init__.py:80
msgid "Kurdish (Kurmanji)"
msgstr "クルド語"

#: dialect/translators/__init__.py:81
msgid "Kyrgyz"
msgstr "キルギス語"

#: dialect/translators/__init__.py:82
msgid "Lao"
msgstr "ラオ語"

#: dialect/translators/__init__.py:83
msgid "Latin"
msgstr "ラテン語"

#: dialect/translators/__init__.py:84
msgid "Latvian"
msgstr "ラトビア語"

#: dialect/translators/__init__.py:85
msgid "Lithuanian"
msgstr "リトアニア語"

#: dialect/translators/__init__.py:86
msgid "Luxembourgish"
msgstr "ルクセンブルク語"

#: dialect/translators/__init__.py:87
msgid "Macedonian"
msgstr "マケドニア語"

#: dialect/translators/__init__.py:88
msgid "Malagasy"
msgstr "マダガスカル語"

#: dialect/translators/__init__.py:89
msgid "Malay"
msgstr "マレー語"

#: dialect/translators/__init__.py:90
msgid "Malayalam"
msgstr "マラヤーラム語"

#: dialect/translators/__init__.py:91
msgid "Maltese"
msgstr "マルタ語"

#: dialect/translators/__init__.py:92
msgid "Maori"
msgstr "マオリ語"

#: dialect/translators/__init__.py:93
msgid "Marathi"
msgstr "マラーティー語"

#: dialect/translators/__init__.py:94
msgid "Mongolian"
msgstr "モンゴル語"

#: dialect/translators/__init__.py:95
msgid "Myanmar (Burmese)"
msgstr "ミャンマー（ビルマ）語"

#: dialect/translators/__init__.py:96
msgid "Nepali"
msgstr "ネパール語"

#: dialect/translators/__init__.py:97
msgid "Norwegian"
msgstr "ノルウェー語"

#: dialect/translators/__init__.py:98
msgid "Odia (Oriya)"
msgstr "オリヤ語"

#: dialect/translators/__init__.py:99
msgid "Pashto"
msgstr "パシュト語"

#: dialect/translators/__init__.py:100
msgid "Persian"
msgstr "ペルシャ語"

#: dialect/translators/__init__.py:101
msgid "Polish"
msgstr "ポーランド語"

#: dialect/translators/__init__.py:102
msgid "Portuguese"
msgstr "ポルトガル語"

#: dialect/translators/__init__.py:103
msgid "Punjabi"
msgstr "パンジャブ語"

#: dialect/translators/__init__.py:104
msgid "Romanian"
msgstr "ルーマニア語"

#: dialect/translators/__init__.py:105
msgid "Russian"
msgstr "ロシア語"

#: dialect/translators/__init__.py:106
msgid "Samoan"
msgstr "サモア語"

#: dialect/translators/__init__.py:107
msgid "Scots Gaelic"
msgstr "スコットランド ゲール語"

#: dialect/translators/__init__.py:108
msgid "Serbian"
msgstr "セルビア語"

#: dialect/translators/__init__.py:109
msgid "Sesotho"
msgstr "ソト語"

#: dialect/translators/__init__.py:110
msgid "Shona"
msgstr "ショナ語"

#: dialect/translators/__init__.py:111
msgid "Sindhi"
msgstr "シンディ語"

#: dialect/translators/__init__.py:112
msgid "Sinhala"
msgstr "シンハラ語"

#: dialect/translators/__init__.py:113
msgid "Slovak"
msgstr "スロバキア語"

#: dialect/translators/__init__.py:114
msgid "Slovenian"
msgstr "スロベニア語"

#: dialect/translators/__init__.py:115
msgid "Somali"
msgstr "ソマリ語"

#: dialect/translators/__init__.py:116
msgid "Spanish"
msgstr "スペイン語"

#: dialect/translators/__init__.py:117
msgid "Sundanese"
msgstr "スンダ語"

#: dialect/translators/__init__.py:118
msgid "Swahili"
msgstr "スワヒリ語"

#: dialect/translators/__init__.py:119
msgid "Swedish"
msgstr "スウェーデン語"

#: dialect/translators/__init__.py:120
msgid "Tajik"
msgstr "タジク語"

#: dialect/translators/__init__.py:121
msgid "Tamil"
msgstr "タミル語"

#: dialect/translators/__init__.py:122
msgid "Tatar"
msgstr "タタール語"

#: dialect/translators/__init__.py:123
msgid "Telugu"
msgstr "テルグ語"

#: dialect/translators/__init__.py:124
msgid "Thai"
msgstr "タイ語"

#: dialect/translators/__init__.py:125
msgid "Turkish"
msgstr "トルコ語"

#: dialect/translators/__init__.py:126
msgid "Turkmen"
msgstr "トルクメン語"

#: dialect/translators/__init__.py:127
msgid "Ukrainian"
msgstr "ウクライナ語"

#: dialect/translators/__init__.py:128
msgid "Urdu"
msgstr "ウルドゥ語"

#: dialect/translators/__init__.py:129
msgid "Uyghur"
msgstr "ウイグル語"

#: dialect/translators/__init__.py:130
msgid "Uzbek"
msgstr "ウズベク語"

#: dialect/translators/__init__.py:131
msgid "Vietnamese"
msgstr "ベトナム語"

#: dialect/translators/__init__.py:132
msgid "Welsh"
msgstr "ウェールズ語"

#: dialect/translators/__init__.py:133
msgid "Xhosa"
msgstr "コサ語"

#: dialect/translators/__init__.py:134
msgid "Yiddish"
msgstr "イディッシュ語"

#: dialect/translators/__init__.py:135
msgid "Yoruba"
msgstr "ヨルバ語"

#: dialect/translators/__init__.py:136
msgid "Zulu"
msgstr "ズールー語"

#~ msgid "Appearance"
#~ msgstr "外観"

#~ msgid "Search Provider"
#~ msgstr "検索プロバイダ"

#~ msgid ""
#~ "To reduce API abuse, the GNOME search provider in Dialect is turned off "
#~ "when Live translation is. Ensure the search provider is also turned on in "
#~ "the GNOME Settings."
#~ msgstr ""
#~ "ライブ翻訳が有効な場合、API の乱用を避けるために Dialect は GNOME 検索で無"
#~ "効になります。GNOME 設定で検索プロバイダが有効になっていることを確認してく"
#~ "ださい。"

#~ msgid "No network connection detected."
#~ msgstr "ネットワークに接続されていません。"
